import axios from "axios"

const BASE_URL = "https://radiant-brushlands-42158.herokuapp.com/api/" 
const TOKEN = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYyOWYyNWRkN2ExYjNhYTA3ODBjMmJhMiIsImlzQWRtaW4iOnRydWUsImlhdCI6MTY1NDYwMTk2NCwiZXhwIjoxNjU0ODYxMTY0fQ.ZJKcPPl3ueN1zDRR9sgRIUHET1kjnQanuCqAXtWjR5w" 

export const publicRequest = axios.create({
    baseURL : BASE_URL
})
export const userRequest = axios.create({
    baseURL : BASE_URL,
    header : {token: `Bearer ${TOKEN}`}
})

// https://damp-stream-25113.herokuapp.com/

//https://radiant-brushlands-42158.herokuapp.com/

// http://localhost:5000/api/

/* 

git =comm

g
*/